class Reaction < ActiveRecord::Base


	def self.calc_breakpoint(drugname=@mic_drug['name'])
   # TODO I will DRY this up once everything is working.
   case true
   when Breakpoint.where(drug: drugname,organism_group_include: @mic_organism['organism_group']).where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?	   
     Breakpoint.where(drug: drugname,organism_group_include: @mic_organism['organism_group']).first
   when Breakpoint.where(drug: drugname,genus_include: @mic_organism['genus']).where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname,genus_include: @mic_organism['genus']).first
   when Breakpoint.where(drug: drugname,master_group_include: @mic_organism['master_group']).where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname,master_group_include: @mic_organism['master_group']).first  
   when Breakpoint.where(drug: drugname).where('organism_code_include LIKE ?', "#{@mic_isolate['organism_code']},%").where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname).where('organism_code_include LIKE ?', "#{@mic_isolate['organism_code']},%").first 
   when Breakpoint.where(drug: drugname).where('organism_code_include LIKE ?', "%,#{@mic_isolate['organism_code']}").where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname).where('organism_code_include LIKE ?', "%,#{@mic_isolate['organism_code']}").first 
   when Breakpoint.where(drug: drugname).where('organism_code_include LIKE ?', "%,#{@mic_isolate['organism_code']},%").where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname).where('organism_code_include LIKE ?', "%,#{@mic_isolate['organism_code']},%")..first 
   when Breakpoint.where(drug: drugname, organism_code_include: @mic_organism['code']).where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname, organism_code_include: @mic_organism['code']).first
   when Breakpoint.where(drug: drugname,level_1_include: @mic_organism['level1']).where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname,level_1_include: @mic_organism['level1']).first
   when Breakpoint.where(drug: drugname,level_3_include: @mic_organism['level3']).where('organism_code_exclude is not ?', "#{@mic_organism['code']}").present?
     Breakpoint.where(drug: drugname,level_3_include: @mic_organism['level3']).first 
  end
end


  def self.calc_reaction(breakpoint)
    value = @mic['value']
    # FIXME  This was just me playing around Jason. I will replace it with a hash lookup
    if @mic['edge'].present?
      value = value * 2 if @mic['edge'] == 1
      value = value / 2 if @mic['edge'] == -1
      value = @mic['value'] if @mic['edge'] == 0
      value = 0.25 if value == 0.24
       value = 0.025 if value == 0.024
      value = 0.0025 if value == 0.0024
    end

    if breakpoint['r_minimum'] == 0 && breakpoint['s_maximum'] == 0
      return reaction = "R"
    elsif breakpoint['r_minimum'].to_f == 0.0 && value >= breakpoint['s_maximum'].to_f 
      return reaction = "S"       
    elsif value > breakpoint['s_maximum'].to_f && value > breakpoint['r_minimum'].to_f && breakpoint['r_minimum'].to_f != breakpoint['s_maximum'].to_f
      return reaction = "R"
    elsif value > breakpoint['s_maximum'].to_f && value < 1 && value < breakpoint['r_minimum'].to_f && breakpoint['s_maximum'].to_f == 0
      return reaction = "S" 
     elsif value > breakpoint['s_maximum'].to_f && value < breakpoint['r_minimum'].to_f && breakpoint['s_maximum'].to_f == 0
      return reaction = "I"
     elsif value <= breakpoint['s_maximum'].to_f
       return reaction = "S"
     elsif value >= breakpoint['r_minimum'].to_f
      return reaction = "R"
     else
      return reaction = "I"
    end
  
  end


  def self.calc_surrogate_reaction(breakpoint)
    all_reactions = []
       surrogate = breakpoint['surrogate_drugs'].split(",").to_a
       surrogate.each{|drugname|
        surbreakpoint = calc_breakpoint(drugname)     
        reactiontest = calc_reaction(surbreakpoint) if surbreakpoint != nil
        all_reactions << reactiontest if reactiontest.present? 
         
      }
         if breakpoint['ni_if_surrogate_is'].present?
            array2 = breakpoint['ni_if_surrogate_is'].split(",").to_a
            ni_if_surrogate_is = array1 & all_reactions
              return "NI" if ni_if_surrogate_is.count > 0
         end

        if breakpoint['r_if_surrogate_is'].present?
            array1 = breakpoint['r_if_surrogate_is'].split(",").to_a
            r_if_surrogate = array1 & all_reactions
              return "R" if r_if_surrogate.count > 0
         end 

      case true
      when all_reactions.exclude?("R")
        return "S"
      when all_reactions.exclude?("S")
        return "R"
      else
        return "S"
      end
  end


 def self.find_reaction(process)
   @mic = Mic.find(process.to_i)
   @mic_drug = Drug.where(drugid: @mic['drug_id']).first
   @mic_isolate = Isolate.where(isolate_id: @mic['isolate_id']).first
   @mic_organism = Organism.where(code: @mic_isolate['organism_code']).first

    if !calc_breakpoint.present?
    	return reaction = "Unknown"
    end

    if !calc_breakpoint['surrogate_drugs'].present?    
     return reaction = calc_reaction(calc_breakpoint) 
    end

    if calc_breakpoint['surrogate_drugs'].present?      
     return reaction = calc_surrogate_reaction(calc_breakpoint)
    end
  end

end
