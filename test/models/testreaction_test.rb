require 'test_helper'

class TestreactionTest < ActiveSupport::TestCase

# TODO Test can be limited by adding limit(200) between all and pluck
  Testreaction.all.pluck(:id).each { | i |
    test "Testing #{ i }" do
      control = Testreaction.find(i)
      skip if Mic.where(drug_id: control['drug_id'], isolate_id: control['isolate_id']).count == 0
      reaction = Testreaction.find_reaction( control )
      assert reaction == control[ 'reaction' ]
    end

  }



end
